
let students = [
    {id:1,name:'sudesh',class1:'10th',city:'borgaon',school:'BHSB',DOB:'3/3/2005'},
    {id:2,name:'yash',class1:'10th',city:'borgaon',school:'BHSB',DOB:'5/5/2005'}
];

generateNextId= ()=>{
	let numberArray = students.map(item=>{return item.id});
	console.log(numberArray);
	//spread operator
	let max = Math.max(...numberArray);
	return max +1;
}

const getStudents = () =>(students);
const addStudent = (record) =>{
    record.id = generateNextId();
    students.push(record);
};
const deleteStudent =({id})=>{
    students = students.filter((item)=>(item.id!=id));
}
const getStudentById=(id)=>{
    let temp = students.filter((item)=>(item.id==id));
    if(temp.length > 0){
        return temp[0];
    }
    return {};
}
const getStudentByName=(name)=>{
    let temp = students.filter((item)=>(item.name == name));
    if(temp.length > 0){
        return temp[0];
    }
    return {};
}

const updateStudent=({id,name,class1,city,school,DOB})=>{
    let temp = students.filter((item)=>(item.id==id));
    if(temp.length > 0){
        let record = temp[0];
        record.name = name;
        record.class1 = class1;
        record.city = city;
        record.school = school;
        record.DOB = DOB;
    }
}
module.exports = {getStudents,addStudent,getStudentById,deleteStudent,updateStudent,getStudentByName};