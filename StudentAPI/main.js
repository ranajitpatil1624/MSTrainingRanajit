const express = require('express'); // not from node
const {getStudents,addStudent,getStudentById,updateStudent,deleteStudent,getStudentByName} = require('./Studentservice'); 

var app = express();
var cors = require('cors') // added 

 
app.use(cors());  // added 
app.use(express.json());

app.get('/api/student', function (req, res) {
	res.send(getStudents());
});

app.get('/api/student/:id', function (req, res) {
    let studentId = req.params.id;
	res.send(getStudentById(studentId));
});
app.get('/api/student/name/:name', function (req, res) {
    let studentName = req.params.name;
	res.send(getStudentByName(studentName));
});

app.delete('/api/student', function (req, res) {
	let student = req.body;
	deleteStudent(student);
	res.send({msg:'Student deleted successfully', result:'success'});
});

app.put('/api/Student', function (req, res) {
	let student = req.body;
	updateStudent(student);
	res.send({msg:'Student updated successfully', result:'success'});
});

app.post('/api/Student', function (req, res) {
	let student = req.body;
	addStudent(student);
	res.send({msg:'Student added successfully', result:'success'});
});

var server = app.listen(3000, function () {
console.log('Express app listening at http://localhost:3000');
});